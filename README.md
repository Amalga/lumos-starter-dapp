# Lumos Starter dApp
This little app was written in co-ordination with a Lab written for the [Nervos Docs](https://docs.nervos.org/docs/labs) labelled **Lumos Framework**.


## Local Development
### Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# pnpm
pnpm install

# Alternatives will work, extra dependencies exist
# to eliminate shamefully hoisting with pnpm

# yarn
yarn install

# npm
npm install
```

## Development Server
Start the development server on http://localhost:3000

```bash
pnpm dev
```

## Production

Build the application for production:

```bash
pnpm build
```

Locally preview production build:

```bash
pnpm preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
